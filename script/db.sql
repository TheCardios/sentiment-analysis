DROP TABLE IF EXISTS "phrases";
DROP SEQUENCE IF EXISTS phrases_id_seq;

CREATE SEQUENCE phrases_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;
CREATE TABLE "public"."phrases" (
    "id" integer DEFAULT nextval('phrases_id_seq') NOT NULL,
    "phrases" text NOT NULL,
    "tag" text NOT NULL,
    CONSTRAINT "phrases_id" PRIMARY KEY ("id")
) WITH (oids = false);