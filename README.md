# sentiment analysis

Progetto realizzato per l'ammissione all esame di Sistemi Esperti e Soft Computing dell'Università E-Campus


## Definizione dei requisiti funzionali e non funzionali 

Creazione di un programma in Python che sarà in grado di effettuare un processo di sentiment analysis che prevede l’inserimento da interfaccia di stringhe in lingua inglese da parte dell'utente o attraverso l’interrogazione di un API.
Lo scopo ultimo del programma sarà sostituire l'utente nella valutazione delle frasi inserite in input: tale meccanismo potrà essere usato come metodo di controllo di messaggi e/o post provenienti da un sito.
Inoltre tutte le stringe valutate dal sistema saranno salvate in un Database in modo che l'utente potrà sempre esportare la base dati aggiornata.
L'intero progetto verrà, poi, reso disponibile mediate immagine Docker come una blackbox in modo da:

-Garantire la massima scalabilità del sistema.

-Evitare installazioni da parte dell’utente, in quanto il programma e la sua base dati risiederanno all'interno dell'immagine Docker.

Il dataset utilizzato sarà di tipo csv e verrà fornito dal sito Kaggle.
Il modello sarà quindi di tipo predittivo ed utilizzerà metodi di classificazione binaria tramite un algoritmo di approccio Bayesiano.
Il sistema verrà addestrato mediante la tecnica del K-fold, in modo da ridurre la dipendenza dai dati.

## Fase di analisi

Il programma presenterà 4 casi d'uso:
### Caso d'uso N1
**Titolo**

**Caricamento del dataset e addestramento del sistema**

**Attori coinvolti: Utente**

In questo caso d'uso l'utente sottopone al sistema il dataset.
In questo progetto il dataset sarà di tipo csv e al suo interno saranno presenti delle recensioni di film etichettate con "pos" per positivo e "neg" per negativo in una colonna apposita denominata TAG.
I dati verranno caricati dal sistema al suo interno per poi essere vettorizzati ed in seguito analizzati.
Al termine di questa fase l’utente verrà avvisato attraverso l’applicativo.


### Caso d'uso N2
**Titolo**

**InQuery da applicativo**

**Attori coinvolti: Utente**

Una volta che il modello è stato addestrato l'utente sarà in grado di effettuare Query su sistema, questo può permettere all'utente di valutare frasi non provenienti dai canali convenzionali tratti nell’esempio (messaggistica/post del sito).
Una volta inviata la query essa viene vettorizzata e poi processata mediante l’algoritmo di approccio Bayesiano scelto.
Successivamente il nuovo valore verrà prima salvato nel database, ed in seguito il risultato verrà visualizzato a video.
In questa fase sono possibili due eccezioni:

•	Il valore analizzato non restituisce il risultato sperato.
Soluzione: all'utente verrà data la possibilità di modificare il valore manualmente e di far ripetere l'addestramento del sistema utilizzando, oltre al dataset, anche la base dati esistente; questo permetterà di aumentare l'efficienza dell’algoritmo.
•	Errore di sistema dovuto all'irraggiungibilità del database.
Soluzione: si richiederà all'utente di riprovare dopo poco, in quanto il database potrebbe essere momentaneamente irraggiungibile.


### Caso d'uso N3
**Titolo**

**InQuery da API esposta**

**Attori coinvolti: Sistema esterno**


Il sistema esporrà un API su protocollo http che richiederà come parametro in ingresso una stringa di tipo Json, dove al suo interno sarà contenuta la stringa da analizzare.
Una volta recepita la stringa essa viene vettorizzata e poi processata mediante algoritmo di approccio Bayesiano scelto.
ll nuovo valore verrà prima salvato nel database e successivamente verrà inviata una risposta di tipo json con il risultato dell'elaborazione del sistema esperto.
In questa fase è possibile solo un’eccezione:

•	Errore di sistema dovuto all'irraggiungibilità del database.
Soluzione: si richiederà al sistema esterno di riprovare dopo poco in quanto il database potrebbe essere momentaneamente irraggiungibile.

### Caso d'uso N4
**Titolo**

**Riaddestramento del sistema esperto**

**Attori coinvolti: Utente**

In questo caso d'uso l'utente richiede al sistema esperto di ripetere la fase di addestramento.
In questa fase oltre ad essere presente il dataset menzionato nel primo caso d'uso, il sistema andrà a prelevare anche tutti i record presenti nel database associato, qualora il database dovesse risultare vuoto, il sistema esperto non procederà con l'addestramento e chiederà ulteriore conferma all’utente nel procedere in un nuovo addestramento. 
In questa fase è possibile solo un’eccezione:

•	Errore di sistema dovuto all'irraggiungibilità del database.
Soluzione: si richiederà all’utente di riprovare dopo poco in quanto il database potrebbe essere momentaneamente irraggiungibile.


## Fase di progetto

### Linguaggio di programmazione:
 Python
### Librerie utilizzate:
- Pandas
- Scikit-Learn

Il programma segue lo standard-package-system definito FastApi in Python, il motore di inferenza è presente all interno della package core.
L'intreffaccia utente è raggiubili all'url di deploy (se in locale localhost) alla porta  8080 con path/docs

Ex in locale:

http://127.0.0.1:8080/docs

La classe del motore inferziale tramite il suo costruttore init iniziallizaiza il modello nel metodo training dove il dataset viene diviso in training e test per poi addestrare il modello sui dati di training per poi confronto le etichette corrette dei dati di test (Y) con le risposte elaborate dal programma (p_test).

sono presenti i seguenti endpoint

```
api_router.include_router(hello.router, prefix="/hello", tags=["hello"])
api_router.include_router(inquery.router, prefix="/inquery", tags=["inquery"])
api_router.include_router(addestramento.router, prefix="/addestramento", tags=["addestramento"])
```

tutto secondo lo schema open-api


## Inizializzazione del progetto in locale

una volta scaricato il progetto recarsi nella cartella app e avviare il pipenv, installare le dipendeze e avviare il progetto dalla classe app.py.
come prima operazione il programma addestrerà il suo modello, ecco il nostro esempio
![](img/init.PNG)

una volta completato l'interfaccia apparirà cosi
![](img/intefaccia.PNG)

saranno presenti 3 endpoint

- il primo per un check di sistema
- il secondo per tutto ciò che riguarda le inquery
- il terzo per eseguire nuvoamente l'addestramento del sistema

si ricorda che tutti gli endpoint possono essere richiamati sia da intefaccia che da api

### Primo Endpoint
![helloPath](img/helloPath.PNG)

Un semplice endpoint di controllo per verificare che il sistema sia salito corettamente

### Secondo Endpoint

qui abbiamo due path possibili, il primo per fare un inquery sul sistema e ottenere il suo risultato

![LOG_inquery_KO](img/inqueryOK.PNG)

di seguito il log
![LogInquert](sent/img/LogInquert)

se il sistema dovesse sbagliare la classificazione all'utente viene data la possibilità di modificare il tag assegno

![inqueryKO](img/inqueryKO.PNG)

in questo caso, essendo la frase in Italiano, il programma non è stato in grado di riconoscerla, di seugito il log

![LogInqueryKO](img/LogInqueryKO.PNG)

L'utente avrà la possibilità di cambiare il tag mediante il path 

> "/confirm" 

utilizzando l'id fornito dal sistema

![confirm](img/update.PNG)

di seguito il log

![LogConf](img/LogUpdate.PNG)

per controllare se la modifica ha avuto veramente successo, bastera richiamare il sistema inserendo la stessa frase, poiche il sistema prima di utilizzare il motore inferenziale controlla la presenza della frase nel Database

![CheckConf](img/CheckUpdate.PNG)


### terzo path addestramento

al terzo path ci sarà la possibilità di ri-addestrare il modello utilizzando anche le frasi presenti nel database, come richiesto se il sistema verficherà che non sono presneti dati aggiunti chiederà un unlteriore conferma all'utente.
di seguito le immagine del processo

![CheckAdd](img/ControlloAddestramento.PNG)

di seguito il log

![LogChecAdd](img/LogControlloAddestramento.PNG)

alla seconda iterazione l'utente conferma la forzatura dell'addestramento

![Addestramento forzato](img/AddestramentoForzato.PNG)

di seugito il log

![LogAddForzato](img/LogAddestramentoForzato.PNG)

se invece sono già presenti delle farsi nel sistema allora l'addestramento procede come segue

![Addestramento forzato](img/AddestramentoForzato.PNG)

di seugito il log

![LogAdd](img/LogAddestramento.PNG)

### Database non raggiubile

qualora il database non fosse raggiubili l'utente verrà informato ed invitato a riprovare.

![](img/erroreDB.PNG)


## Concolusioni

Il seguente programma può essere una buona base di partenza per il cehck delle recsioni/post/messaggi che vengono inviati ad un sito.
Con un dataset più ampio si potrebbe anche aumentare l'affidabilità del sistema, ma ritengo che una percetuale di quasi l'80% possa essere accettata.
