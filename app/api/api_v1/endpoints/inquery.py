from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from api import deps
from core.inference_engine import motore_inf
from crud.crud_phrase import crud_phrases
from models.phrases import Phrases
from schemas.phrase import PhraseCreate, PhraseUpdate

router = APIRouter()


@router.post("/", response_model=str)
def create_phrase(phrase: PhraseCreate, db: Session = Depends(deps.get_db)) -> str:
    """
    Create user if not exists
    """
    phrase_in_db = crud_phrases.get_phrase_by_phrase(db, phrase.phrase)
    print("controllo nel db")
    if phrase_in_db:
        print("frase gia presente")
        raise HTTPException(status_code=200, detail="Phrases already exists with tag " + phrase_in_db.tag) 
    print("frase non presnete")
    tag = motore_inf.in_query(phrase.phrase)
    print("tag: " + tag[0])
    phrase.tag = tag[0]
    p = crud_phrases.create_phrase(db , phrase)
    return str(str("id: ")+str(p.id) + str(" ") + str("tag: ") + str(p.tag))
    print("error")
    return "database non raggiunginile ripova più tardi"


@router.post("/confirm", response_model=str)
def confirm_phrase(phrase: PhraseUpdate, db: Session = Depends(deps.get_db)) -> str:
    "Confimr phrase"
    print("inioz update")
    try:
        crud_phrases.update_phrase(db, phrase , phrase.id)
        return "ok"
    except:
        print("error")
        return "database non raggiunginile ripova più tardi"