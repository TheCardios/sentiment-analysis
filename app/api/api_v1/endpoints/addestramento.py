from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from api import deps
from crud.crud_phrase import crud_phrases
from core.inference_engine import motore_inf


router = APIRouter()


@router.get("/", response_model=str)
def restart_addestramento(db: Session = Depends(deps.get_db)):
    """
    addestramento
    """
    print("addestramento")
    try:
        if motore_inf.get_init() == 0:
            print("non ci sono frasi")
            motore_inf.forza_riadd()
            print(motore_inf.get_init())
            return "non ci sono valori aggiunti si vuole continuare?"
        phrases = crud_phrases.get_phrases(db)
        return motore_inf.ri_addestramento(phrases)
    except:
        print("error")
        return "database non raggiunginile ripova più tardi"
