from typing import Generic, TypeVar, Type


ModelType = TypeVar("ModelType")


class CRUDBase(Generic[ModelType]):
    """
    CRUDBase class
    """

    def __init__(self, model: Type[ModelType]):
        """
        CRUD object with default methods to Create, Read, Update, Delete (CRUD).
        **Parameters**
        * `model`: A SQLAlchemy model class
        * `schema`: A Pydantic model (schema) class
        """
        self.model = model
