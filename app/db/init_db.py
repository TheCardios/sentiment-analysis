from sqlalchemy.orm import Session
from db.database import engine
from models import phrases


def init_db(db: Session) -> None:
    """
    Create dynamically db
    Create admin user if not exists
    """
    phrases.Phrases.metadata.create_all(bind=engine)
