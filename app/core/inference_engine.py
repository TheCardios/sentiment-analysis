import pandas as pd
from numpy import random, array
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import BernoulliNB
from sklearn.metrics import accuracy_score


class INFERENCEengine():
    """
    inference class
    """
    model = BernoulliNB()
    init = 0
    vector = CountVectorizer(ngram_range=(1, 2))

    def __init__(self):
        print("init motore inferenza")
        INFERENCEengine.start_training()

    @classmethod
    def start_training(cls):
        phrase, tag = INFERENCEengine.get_pharase_and_tag()
        INFERENCEengine.training(phrase, tag)

    @classmethod
    def training(cls, phrase, tag):
        random.seed(0)
        phrase = INFERENCEengine.vector.fit_transform(phrase)
        print("Divido il dataset in training e test")
        phrase_train, phrase_test, tag_train, tag_test = train_test_split(phrase, tag, test_size=0.01)
        print("addestramento sui dati di training")
        INFERENCEengine.model.fit(phrase_train, tag_train)
        print("ottengo i risultati sui di test")
        p_test = INFERENCEengine.model.predict(phrase_test)
        print("confronto le etichette corrette dei dati di test (Y) con le risposte elaborate dal programma (p_test)")
        error = accuracy_score(tag_test, p_test)
        print("il test ha portato come percentuale di accuratezza il: " + str(error*100))

    @classmethod
    def get_pharase_and_tag(cls):
        df = pd.read_csv('app/resource/movie_review.csv')
        phrase = df['text']
        tag = df['tag']
        return phrase, tag

    def in_query(self, phrases: str):
        print("inizio inquery")
        print(phrases)
        q = array([phrases])
        print("settato q")
        q = INFERENCEengine.vector.transform(q)
        print("vettorizzato q")
        INFERENCEengine.init = 1
        print("init settato")
        preditct = INFERENCEengine.model.predict(q)
        print("predict : " + str(preditct))
        return preditct

    def ri_addestramento(self, phrases_db):
        print("start ri_addestramento")
        new_model = BernoulliNB()
        INFERENCEengine.model = new_model
        phrase, tag = INFERENCEengine.get_pharase_and_tag()
        print("ottengo le frasi e i tag")
        if INFERENCEengine.init == 1:
            print("ci sono frasi")
            for i in phrases_db:
                phrase.append(pd.Series([i.phrases]))
                tag.append(pd.Series([i.tag]))
            INFERENCEengine.training(phrase, tag)
        elif INFERENCEengine.init == 2:
            print("riaddestramento forzato")
            INFERENCEengine.init = 0
            print("reseto forzatura")
            INFERENCEengine.training(phrase, tag)
            return "ok" 
        else: 
            print("non ci sono frasi")
            random.seed(0)
            INFERENCEengine.training(phrase, tag)
            return "ok"

    def get_init(self):
        print("get init")
        return INFERENCEengine.init
        
    def forza_riadd (self):
        INFERENCEengine.init = 2


motore_inf = INFERENCEengine()
